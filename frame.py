from tkinter import *
import speech_recognition as sr
import cv2
import numpy as np
import pyttsx3

def voz(a):
    engine = pyttsx3.init()
    if a == 1:
        engine.say("Azul")
    if a == 2:
        engine.say("Amarillo")
    if a == 3:
        engine.say("Rojo")
    engine.runAndWait()


def dibujar(mask, color, frame):

    contornos,_ =cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for c in contornos:
        a = 0;
        area=cv2.contourArea(c)
        if area > 3000:
            nuevo= cv2.convexHull(c)
            cv2.drawContours(frame, [nuevo], 0, color, 3)
            if color == [255,0,0]:
                print("azul")
                a = 1
                voz(a)
            if color == [0,255,255]:
                print("amarillo")
                a = 2
                voz(a)
            if color == [0,0,255]:
                print("rojo")
                a = 3
                voz(a)
    #cv2.imshow('maskAzul', mask)
def color():
    cap = cv2.VideoCapture(0)

    azulBajo = np.array([100,100,20],np.uint8)
    azulAlto = np.array([125,255,255],np.uint8)

    amaB = np.array([15,100,20],np.uint8)
    amaA = np.array([45,255,255],np.uint8)

    redB = np.array([0,100,20],np.uint8)
    redA = np.array([5,255,255],np.uint8)

    redB2 = np.array([175,100,20],np.uint8)
    redA2 = np.array([179,255,255],np.uint8)
    font = cv2.FONT_HERSHEY_SIMPLEX
    while True:

      ret,frame = cap.read()

      if ret==True:
        frameHSV = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        maskAz = cv2.inRange(frameHSV,azulBajo,azulAlto)
        maskAm = cv2.inRange(frameHSV, amaB, amaA)
        maskR1 = cv2.inRange(frameHSV, redB, redA)
        maskR2 = cv2.inRange(frameHSV, redB2, redA2)
        maskR = cv2.add(maskR1, maskR2)

        dibujar(maskAz, [255,0,0], frame)
        dibujar(maskAm, [0,255,255], frame)
        dibujar(maskR, [0,0,255], frame)

        cv2.imshow('frame', frame)

        if cv2.waitKey(1) & 0xFF == ord('s'):
          break
    cap.release()
    cv2.destroyAllWindows()

ventana = Tk() #creamos la ventana
ventana.title("Resume el texto") #agregamos titulo a la ventana
ventana.resizable(0,0) #no se puede cambiar el tamaño
ventana.geometry("800x500") #tamaño de la ventana

lbl1 = Label(ventana, text="Reconocimiento RGB ") #creamos una etiqueta con texto
lbl1.place(x=350, y=10)

btn1 = Button(ventana, text="Iniciar cámara", command=lambda: color())
btn1.place(x=10, y=30, width=780, height = 30)

lbl2 = Label(ventana, text="El color es: ") #creamos una etiqueta con texto
lbl2.place(x=350, y=470)


ventana.mainloop();
